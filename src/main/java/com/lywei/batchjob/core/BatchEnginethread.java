package com.lywei.batchjob.core;

import service.JobInfoService;

/**
 * Created by austin on 11/24/16.
 */
public class BatchEnginethread implements Runnable{

    private JobInfoService jobInfoService;
    @Override
    public void run() {
        initBatchJob();

        while (true)
        {
            observe();
            try {
                Thread.sleep(1000 * 60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 第一次启动时, 初始化所有Job
     */
    private void initBatchJob(){
        jobInfoService.updateJobInStatus(BatchJobConstant.JOB_STATUS_NEW);
    }

    /**
     * 查找最新需要schedule的任务
     */
    private void observe(){

    }
}
