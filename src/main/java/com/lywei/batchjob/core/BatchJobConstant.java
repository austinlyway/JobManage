package com.lywei.batchjob.core;

/**
 * Created by austin on 11/25/16.
 */
public class BatchJobConstant {

    public static final int JOB_STATUS_NEW = 1;

    public static final int JOB_STATUS_SCHEDULED = 2;

    public static final int JOB_STATUS_STOPED = 3;

    public static final int JOB_STATUS_NOT_START = 0;

}
