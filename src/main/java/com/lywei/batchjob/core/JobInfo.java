package com.lywei.batchjob.core;

import javax.persistence.Column;
import java.util.Date;

/**
 * Created by austin on 11/24/16.
 */
public class JobInfo {

    private Integer jogId;

    private String jobName;

    /**任务状态**/
    private Integer jobStatus;
    /**任务开始时间**/
    private Date startTime;
    /**任务结束时间**/
    private Date endTime;
    /**执行类型: 1: 一次, 2: 循环**/
    private Integer runType;
    /**运行的时间: 当type是一次性的时候: 为时间串, 当type为重复的时候, 为cron表达式**/
    private String runTime;
    /**优先级, 数字越小优先级越大**/
    private Integer priority;
    /****/
    private Date createdTime;
    /****/
    private String createdBy;
    /****/
    private Date modifiedTime;
    /****/
    private String modifiedBy;
    /**任务处理类的路径**/
    private String handlerClassPath;
    /**通知者的电话**/
    private String notifyMobile;
    /**通知者的邮箱**/
    private String notifyEmail;

    @Column(name = "JOB_ID")
    public Integer getJogId() {
        return jogId;
    }

    public void setJogId(Integer jogId) {
        this.jogId = jogId;
    }

    @Column(name = "JOB_NAME")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Column(name = "JOB_STATUS")
    public Integer getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(Integer jobStatus) {
        this.jobStatus = jobStatus;
    }

    @Column(name = "START_TIME")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Column(name = "END_TIME")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Column(name = "RUN_TYPE")
    public Integer getRunType() {
        return runType;
    }

    public void setRunType(Integer runType) {
        this.runType = runType;
    }

    @Column(name = "RUN_TIME")
    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    @Column(name = "PRIORITY")
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Column(name = "CREATED_TIME")
    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "MODIFIED_TIME")
    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Column(name = "MODIFIED_BY")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "HANDLER_CLASS_PATH")
    public String getHandlerClassPath() {
        return handlerClassPath;
    }

    public void setHandlerClassPath(String handlerClassPath) {
        this.handlerClassPath = handlerClassPath;
    }

    @Column(name = "NOTIFY_MOBILE")
    public String getNotifyMobile() {
        return notifyMobile;
    }

    public void setNotifyMobile(String notifyMobile) {
        this.notifyMobile = notifyMobile;
    }

    @Column(name = "NOTIFY_EMAIL")
    public String getNotifyEmail() {
        return notifyEmail;
    }

    public void setNotifyEmail(String notifyEmail) {
        this.notifyEmail = notifyEmail;
    }
}
