package com.lywei.batchjob.core;

import java.util.Date;

/**
 * Created by austin on 11/24/16.
 */
public class JobLog {

    /****/
    private Integer jogLobId;

    private Integer jobId;
    /**运行开始时间**/
    private Date runStartTime;
    /**运行结束时间**/
    private Date runEndTime;
    /**运行的主机IP**/
    private String serverId;
    /**运行结果, 1: 成功, -1:失败**/
    private Integer result;
    /**异常信息日志**/
    private String exceptionLog;
    /**运行花费时间, 单位:s **/
    private Integer timeConsume;
    /**任务驱动运行方式, 1: 自动, 2: 手动**/
    private Integer triggerType;
    /**运行参数**/
    private String params;

    public Integer getJogLobId() {
        return jogLobId;
    }

    public void setJogLobId(Integer jogLobId) {
        this.jogLobId = jogLobId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Date getRunStartTime() {
        return runStartTime;
    }

    public void setRunStartTime(Date runStartTime) {
        this.runStartTime = runStartTime;
    }

    public Date getRunEndTime() {
        return runEndTime;
    }

    public void setRunEndTime(Date runEndTime) {
        this.runEndTime = runEndTime;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getExceptionLog() {
        return exceptionLog;
    }

    public void setExceptionLog(String exceptionLog) {
        this.exceptionLog = exceptionLog;
    }

    public Integer getTimeConsume() {
        return timeConsume;
    }

    public void setTimeConsume(Integer timeConsume) {
        this.timeConsume = timeConsume;
    }

    public Integer getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(Integer triggerType) {
        this.triggerType = triggerType;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}
