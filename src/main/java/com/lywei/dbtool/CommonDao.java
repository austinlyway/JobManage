package com.lywei.dbtool;

import com.lywei.dbtool.annotation.AnnotatedClass;
import com.lywei.dbtool.annotation.AnnotatedProperty;
import com.lywei.dbtool.annotation.AnnotationConfigurationUtil;
import com.lywei.dbtool.sql.SQLBuilder;
import com.lywei.dbtool.sql.SQLObject;
import com.lywei.util.EmptyUtil;

import javax.persistence.IdClass;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CommonDao
{


	/**
	 * 将List<Map>数组转换成List<Object>对象数组,转换的对象类型由传入的classType确定
	 * 
	 * @param src
	 * @param classType
	 * @return
	 */
	private <T> List<T> mapToEntities(List<Map<String, Object>> src, Class<T> classType)
	{
		List<T> des = new ArrayList<T>();
		if (src == null || src.isEmpty())
		{
			return des;
		}
		for (int i = 0, length = src.size(); i < length; i++)
		{
			des.add(mapToEntity(src.get(i), classType));
		}
		return des;
	}

	/**
	 * 将具体的map转换成Entity对象
	 * 
	 * @param src
	 * @param classType
	 * @return
	 */
	private <T> T mapToEntity(Map<String, Object> src, Class<T> classType)
	{
		T entity = null;
		try
		{
			AnnotatedClass annotatedClass = new AnnotatedClass(classType);
			entity = classType.newInstance();
			if (src == null)
			{
				return entity;
			}
			Iterator<String> columnNamekey = src.keySet().iterator();
			//对象中变量名
			Field field = null;
			String columnName;
			AnnotatedProperty idAnnotaion = annotatedClass.getEmbeddedId();
			// 1. 递归处理EmbeddedId
			if(idAnnotaion != null)
			{
				Object idObject = mapToEntity(src, idAnnotaion.getType());
				field = idAnnotaion.getField();
				field.setAccessible(true);
				field.set(entity, idObject);
			}
			//2. 遍历处理查询中的每一个字段
			while (columnNamekey.hasNext())
			{
				columnName = columnNamekey.next();
				// 如果字段上面有对应的column字段名, 则直接对应.
				AnnotatedProperty annotatedProperty = annotatedClass.getPropertyByColumn(columnName);
				// 获得对应的字段
				if(annotatedProperty != null)
				{
					field = annotatedProperty.getField();
				}
				// 根据名字忽略小写来Mapping字段
				else
				{
					field = getMapColumn2FieldByName(src, columnName, entity);
				}
				if(field != null)
				{
					setValue4Entity(field, entity, getObjectByColumnName(src, columnName, field.getType()));
				}

			}
			return entity;
		}
		catch (Exception e)
		{
			throw new RuntimeException("无法转换DB数据为对象实例," + e.getMessage(), e);
		}
	}


	/**
	 * 根据表字段名和类字段名, 名字一样来匹配设置
	 */
	private <T> Field getMapColumn2FieldByName(Map<String, Object> src, String columnName, T entity) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		//对象中变量名
		Field[] fieldArray = entity.getClass().getDeclaredFields();
		Field field = null;
		Field resultField = null;
		int length = fieldArray.length;
		for (int i = 0; i < length; i++)
		{
			field = fieldArray[i];
			if (field.getName().startsWith("ALIAS_") || field.getName().equals("serialVersionUID")) {
				resultField = null;
				continue;
			}
			if (field.getName().equalsIgnoreCase("id")) {
				resultField = null;
				continue;
			}
			if ((columnName.replace("_", "")).equalsIgnoreCase(field.getName())) {
				resultField = field;
				break;
			}
		}
		return resultField;
	}

	/**
	 * 讲数据查询的值,设置到对应的对象实例
	 * @param field
	 * @param entity
	 * @param value
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private void setValue4Entity(Field field, Object entity, final Object value) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		if(value == null)
		{
			return;
		}
		Object realValue = value;
		field.setAccessible(true);
		Class<?> toClazz = field.getType();
		//特殊处理继承自PersistEnum的属性
		field.set(entity, realValue);
	}

	/**
	 * 根据数据库列名,查询数据的数据
	 * @param columnValueMap
	 * @param columnName
	 * @param toClazz
	 * @return
	 */
	private Object getObjectByColumnName(Map<String, Object> columnValueMap, String columnName, Class<?> toClazz)
	{
		if(columnValueMap == null || EmptyUtil.isEmpty(columnName))
		{
			return null;
		}
		Object value = columnValueMap.get(columnName);
		// 当数据库值的类型与Java目标类型不一致的时候,进行类型转换
		if(toClazz != null && value != null && value.getClass() != toClazz)
		{
			if (toClazz == Double.class)
			{
				value = Double.parseDouble(value.toString());
			}
			if(toClazz == Integer.class)
			{
				value = Integer.parseInt(value.toString());
			}
			if(toClazz == String.class)
			{
				value = String.valueOf(value);
			}
		}
		return value;
	}

	/**
	 * created by chenjin 2013-08-07 update Object 更新实体对象,如果对象属性为空,则不做处理
	 * @throws Exception 
	 */
	public void updateIgnoreNull(Object entity)
	{
		// 传入对象不能为空
		if(entity == null){
			throw new IllegalArgumentException("entity is required!");
		}
		SQLObject sqlObj = SQLBuilder.buildUpdateSQL(entity, getPK(entity), false);
//		executeUpdate(sqlObj.getSql(), sqlObj.getParameters());
	}


	/**
	 * 
	 * Return PK object from domain model.
	 *
	 * @param domainModel	domain model.
	 * @return
	 * @
	 */
	public static Object getPK(Object domainModel)
	{
		Object pkObj = null;
		Class<?> clazz = domainModel.getClass();
		IdClass idClazz = clazz.getAnnotation(IdClass.class);
		if (idClazz == null)
		{
			AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(clazz);
			AnnotatedProperty annotatedProperty = annotatedClass.getEmbeddedId();
			if (annotatedProperty == null)
			{
				//get ID list
				List<AnnotatedProperty> idList = annotatedClass.getIds();
				//Null check
				if (idList != null)
				{
					//Only one ID annotation as PK
					if (idList.size() == 1)
					{
						pkObj = idList.get(0).getValue(domainModel);
					}
					//Domain model extends PK model
					else
					{
						pkObj = domainModel;
					}
				}
			}
			//Embedded ID is sub model in domain model.
			else
			{
				pkObj = annotatedProperty.getValue(domainModel);
			}
		}
		else
		{
			Class<?> pkClass = idClazz.value();
			//3. Domain model extends PK model.
			if (pkClass.isAssignableFrom(clazz))
			{
				pkObj = domainModel;
			}
			//4. It has an embeddedID object.
			else
			{
				AnnotatedProperty annotatedProperty = AnnotationConfigurationUtil.getClassAnnotation(clazz).getEmbeddedId();
				//Embedded ID is sub model in domain model.
				if (annotatedProperty != null)
				{
					pkObj = annotatedProperty.getValue(domainModel);
				}
			}
		}
		if (pkObj == null)
		{
			throw new RuntimeException("Not Found PK object from " + clazz.getName());
		}
		return pkObj;
	}


//	/**
//	 * 执行原生的SQL语句
//	 *
//	 * @param exeSQL
//	 * @param params
//	 */
//	public void executeUpdateSQL(final String exeSQL, final Object[] params)
//	{
//		if(EmptyUtil.isNotEmpty(exeSQL)){
//			throw new IllegalArgumentException("sql statement is required!");
//		}
//		final String fnSQL = preHandleSQL(exeSQL, params);
//		try
//		{
//			new SpringJdbcDBAccessor(this.getHibernateTemplate().getSessionFactory()).update(fnSQL, params);
//		}
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
//	}

	private void printSQLParameter(String sql, Object[] parameter)
	{
		StringBuilder parameterStr = new StringBuilder(200);
		if (null != parameter && parameter.length > 0)
		{
			parameterStr.append("[");
			for (int index = 0; index < parameter.length; index++)
			{
				if (index > 0)
				{
					parameterStr.append(",");
				}
				parameterStr.append(parameter[index]);
			}
			parameterStr.append("]");
			return;
		}
		parameterStr.append("No Parameters");
	}

}
