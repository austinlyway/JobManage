package com.lywei.dbtool;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.lywei.dbtool.jdbc.DefaultResultConverterImpl;
import com.lywei.dbtool.jdbc.ResultConverter;
import com.lywei.dbtool.jdbc.ResultConverterImpl;

import com.alibaba.druid.pool.DruidDataSource;
import com.mysql.jdbc.Statement;

public class JdbcDBAccessor
{

	private DruidDataSource druidDataSource;


	public DruidDataSource getBasicDataSource()
	{
		return druidDataSource;
	}

	public void setBasicDataSource(DruidDataSource druidDataSource)
	{
		this.druidDataSource = druidDataSource;
	}

	public <T> List<T> query(String sql, Object[] param, ResultConverter<T> converter) throws RuntimeException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<T> records = new ArrayList<T>();
		try
		{
			conn = getConnection();
			pst = conn.prepareStatement(sql);
			if (param != null)
			{
				setParameters(pst, param, conn);
			}
			rs = pst.executeQuery();

			if (converter == null)
			{
				converter = new DefaultResultConverterImpl();
			}

			while (rs.next())
			{
				T modelObj = converter.convert(rs);
				records.add(modelObj);
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("Failed to retrieve data.", e);
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
				}

				if (pst != null)
				{
					pst.close();
				}
			}
			catch (SQLException e)
			{
			}
		}

		return records;
	}
	

	public int count(String sql, Object[] param) throws RuntimeException
	{
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int results = 0;
		try
		{
			conn = getConnection();
			pst = conn.prepareStatement(sql);
			if (param != null)
			{
				setParameters(pst, param, conn);
			}
			rs = pst.executeQuery();
			rs.next();
			results = rs.getInt(1);
		}
		catch (Exception e)
		{

			throw new RuntimeException("Failed to retrieve data.", e);
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
				}

				if (pst != null)
				{
					pst.close();
				}

			}
			catch (SQLException e)
			{
			}
		}

		return results;
	}

	public int update(String sql, Object[] param) throws RuntimeException
	{
		return update(null, sql, param);
	}

	public int update(Connection externalConn, String sql, Object[] param) throws RuntimeException
	{
		Connection conn = null;
		PreparedStatement pst = null;

		Integer result = 0;
		try
		{
			if (externalConn == null)
			{
				conn = getConnection();
			}
			else
			{
				conn = externalConn;
			}

			pst = conn.prepareStatement(sql);
			if (param != null)
			{
				setParameters(pst, param, conn);
			}
			result = pst.executeUpdate();

		}
		catch (Exception e)
		{

			throw new RuntimeException("Failed to update data.", e);
		}
		finally
		{
			try
			{
				if (pst != null)
				{
					pst.close();
				}

			}
			catch (SQLException e)
			{
			}
		}
		return result;

	}

	public Object updateReturnPrimaryKey(String sql, Object[] param) throws RuntimeException
	{
		Connection conn = null;
		PreparedStatement pst = null;

		Object result = 0;
		try
		{
			conn = getConnection();
			
			pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			if (param != null)
			{
				setParameters(pst, param, conn);
			}

			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();

			if (rs.next())
			{
				result = rs.getString(1);
			}

		}
		catch (Exception e)
		{
			throw new RuntimeException("Failed to update data.", e);
		}
		finally
		{
			try
			{
				if (pst != null)
				{
					pst.close();
				}
			}
			catch (SQLException e)
			{
			}
		}
		return result;

	}

	public int batchUpdate(String sql, List<Object[]> params) throws RuntimeException
	{
		Connection conn = null;
		PreparedStatement pst = null;

		int affectCount = 0;
		try
		{
			conn = getConnection();
			pst = conn.prepareStatement(sql);

			for (int i = 0; i < params.size(); i++)
			{
				setParameters(pst, params.get(i), conn);
				pst.addBatch();
			}

			int[] resultList = pst.executeBatch();
			for (int count : resultList)
			{
				affectCount = affectCount + count;
			}

			if (affectCount <= -2)
			{
				affectCount = pst.getUpdateCount();
			}

		}
		catch (Exception e)
		{
			throw new RuntimeException("Failed to update data.", e);
		}
		finally
		{
			try
			{
				if (pst != null)
				{
					pst.close();
				}

			}
			catch (SQLException e)
			{
				throw new RuntimeException("Failed to close db resources.", e);
			}
		}

		return affectCount;
	}

	public static void setParameters(PreparedStatement pstmt, Object[] parameters, Connection conn) throws SQLException
	{
		if (parameters != null)
		{
			for (int i = 0; i < parameters.length; i++)
			{
				if (parameters[i] == null || "".equals(parameters[i]))
				{
					pstmt.setString(i + 1, null);
				}
				else if (parameters[i] instanceof java.sql.Date)
				{
					pstmt.setDate(i + 1, (java.sql.Date) parameters[i]);
				}
				else if (parameters[i] instanceof java.util.Date)
				{
					pstmt.setTimestamp(i + 1, new Timestamp(((java.util.Date) parameters[i]).getTime()));
				}
				else if (parameters[i] instanceof InputStream)
				{
					try
					{
						// if the input stream is empty, set to null.
						if (((InputStream) parameters[i]).available() == 0)
						{
							pstmt.setBinaryStream(i + 1, null, 0);
						}
						// Set InputSteam
						else
						{
							pstmt.setBinaryStream(i + 1, (InputStream) parameters[i], ((InputStream) parameters[i]).available());
						}
					}
					catch (IOException e)
					{
						throw new SQLException(e);
					}
				}
				else
				{
					pstmt.setObject(i + 1, parameters[i]);
				}
			}
		}
	}

	public Connection getConnection()
	{
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://120.76.84.126:3306/yitian_db_dev?characterEncoding=utf8";
		String username = "root";
		String password = "youxinbao";
		Connection conn = null;
		try {
			Class.forName(driver); //classLoader,加载对应驱动
			conn = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	private void closeConnect(){

	}
}
