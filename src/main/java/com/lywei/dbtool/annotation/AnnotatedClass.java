package com.lywei.dbtool.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * <pre>
 */
public class AnnotatedClass
{
	/** The CGLIB class separator character "$$" */
	private static final String CGLIB_CLASS_SEPARATOR = "$$";
	
	private transient Class<?> clazz;

	/**
	 * The relationship between property name and AnnotatedProperty without embedded class. Map<propertyName,
	 * AnnotatedProperty>
	 */
	private Map<String, AnnotatedProperty> propertyList = new TreeMap<String, AnnotatedProperty>();

	/**
	 * <pre>
	 * Relationship between Column name and property.
	 * Map&lt;ColumnName, property&gt;
	 * There are key/value pair, the key is DB column name, 
	 * 1. if it is property, the value is AnnotatedProperty,
	 * 2, if it is the property of embedded model, 
	 * the value is String, such as capTypePK.serviceProviderCode.
	 * </pre>
	 */
	private Map<String, AnnotatedProperty/*AnnotatedProperty*/> columnMap = new LinkedHashMap<String, AnnotatedProperty>();
	private Map<String/*propertyName*/, AnnotatedProperty/**/> propertyColumnAnnMap = new LinkedHashMap<>();
	
	/**
	 *  transient Column Map
	 */
	private Map<String, String> transientColumnMap = null;
	/**
	 * Lob column map
	 */
	private Map<String, String> lobColumnMap = null;
	/**
	 * table definition
	 */
	private Table table;
	/**
	 * Primary key for conflict detect.
	 */
	/**
	 * If one sequence column is in PK.
	 */
	private boolean isSeqenceInPK;

	/**
	 * Identity with composite primary key
	 */
	private AnnotatedProperty embeddedId;

	private AnnotatedProperty yitianId;
	/**
	 * List all ID properties.
	 */
	private List<AnnotatedProperty> ids ; 
	/**
	 * PK column list
	 */
	private String[] pkColumns;

	/**
	 * List all properties to be declared with Embedded or EmbeddedId for better performance.
	 */
	private List<AnnotatedProperty> embeddedProperties = new ArrayList<AnnotatedProperty>();
	/**
	 * List all properties to be declared with ManyToOne, ManyToMany, OneToMany, OneToOne or AccelaQuery.
	 */
	private List<AnnotatedProperty> subModelProperties = new ArrayList<AnnotatedProperty>();

	/**
	 * AnnotatedClass Constructor. 
	 *
	 * @param clazz class
	 */
	public AnnotatedClass(Class<?> clazz)
	{
		this.clazz = clazz;
		if (clazz.getName().indexOf(CGLIB_CLASS_SEPARATOR) != -1)
		{
			this.clazz = clazz.getSuperclass();
		}
		// Get all declared getter methods and annotation.
		Method[] publicMethods = clazz.getMethods();
		for (Method method : publicMethods)
		{
			// It is getter method ?
			if (isGetter(method))
			{
				Annotation[] annotations = method.getAnnotations();
				// It is annotated method ?
				if (annotations != null && annotations.length > 0)
				{
					putPropertyInCache(method);
				}
			}
		}
		init();
	}
	
	private void putPropertyInCache(Method getterMethod)
	{
		Method setterMethod = getSetterMethod(clazz, getterMethod);
		AnnotatedProperty property = new AnnotatedProperty(getterMethod, setterMethod, clazz);
		propertyList.put(property.getName(), property);
	}

	/**
	 * Get annotation for use later.
	 */
	public void init()
	{
		table = this.clazz.getAnnotation(Table.class);
		Iterator<Map.Entry<String, AnnotatedProperty>> allProperty = propertyList.entrySet().iterator();
		while (allProperty.hasNext())
		{
			AnnotatedProperty property = allProperty.next().getValue();
			classifyProperty(property);
		}
		initColumnsInClass();
	}

	/**
	 * Cache Embeddedid and id property
	 * @param property
	 */
	private void classifyProperty(AnnotatedProperty property)
	{
		EmbeddedId embeddedId1 = property.getAnnotation(EmbeddedId.class);
		Embedded embedded = property.getAnnotation(Embedded.class);
		Transient transientp =  property.getAnnotation(Transient.class);
		Id id1 = property.getAnnotation(Id.class);
		if (transientp == null && embeddedId1 != null)
		{
			embeddedId = property;
		}
		if (transientp == null && id1 != null)
		{
			if (ids == null)
			{
				ids = new ArrayList<AnnotatedProperty>();
			}
			//Support composite PK columns.
			ids.add(property);
		}
		if (transientp == null && (embeddedId1 != null || embedded != null))
		{
			embeddedProperties.add(property);
		}
		if (yitianId != null)
		{
			this.yitianId = property;
		}
	}
	

	private void initColumnsInClass()
	{
		// 1. Loop to add all columns.
		Iterator<AnnotatedProperty> allProperties = propertyList.values().iterator();
		while (allProperties.hasNext())
		{
			AnnotatedProperty annotatedProperty = allProperties.next();
			Column columnObj = annotatedProperty.getAnnotation(Column.class);
			if (columnObj != null && columnObj.name() != null && columnObj.name().length() > 0)
			{
				columnMap.put(columnObj.name(), annotatedProperty);
				propertyColumnAnnMap.put(annotatedProperty.getName(), annotatedProperty);
				if (annotatedProperty.getAnnotation(Transient.class) != null)
				{
					if (transientColumnMap == null)
					{
						transientColumnMap = new HashMap<String, String>();
					}
					transientColumnMap.put(columnObj.name(), columnObj.name());
				}
				if (annotatedProperty.getAnnotation(Lob.class) != null)
				{
					if (lobColumnMap == null)
					{
						lobColumnMap = new HashMap<String, String>();
					}
					lobColumnMap.put(columnObj.name(), columnObj.name());
				}
			}
		}
	}
	/**
	 * 
	 * Get all columns definition in embedded class.
	 *
	 * @param embeddedProperty	embedded property
	 * @param embeddedClass		embedded class
	 */
	public void initColumnsInEmbeddedClass(AnnotatedProperty embeddedProperty,
			AnnotatedClass embeddedClass)
	{
		if (embeddedClass != null)
		{
			// 1. Loop to add all columns.
			Iterator<AnnotatedProperty> propertiesInEmbeddedClass = embeddedClass.propertyList.values().iterator();
			while (propertiesInEmbeddedClass.hasNext())
			{
				AnnotatedProperty subProperty = propertiesInEmbeddedClass.next();
				subProperty.setParentAnnotatedProperty(embeddedProperty);
				Column columnObj = subProperty.getAnnotation(Column.class);
				if (columnObj != null && columnObj.name() != null && columnObj.name().length() > 0)
				{
					// For embedded class, it will find a String, such as capTypePK.group
					String columnName = getRealColumnName(embeddedProperty, subProperty, columnObj);
					columnMap.put(columnName, subProperty);
					if (subProperty.getAnnotation(Transient.class) != null)
					{
						if (transientColumnMap == null)
						{
							transientColumnMap = new HashMap<String, String>();
						}
						transientColumnMap.put(columnObj.name(), columnObj.name());
					}
					if (subProperty.getAnnotation(Lob.class) != null)
					{
						if (lobColumnMap == null)
						{
							lobColumnMap = new HashMap<String, String>();
						}
						lobColumnMap.put(columnObj.name(), columnObj.name());
					}
				}
			}
		}
	}

	private String getRealColumnName(AnnotatedProperty embeddedProperty, AnnotatedProperty subProperty, Column columnObj)
	{
		Column realColumn = columnObj;
		AttributeOverrides overrides= embeddedProperty.getAnnotation(AttributeOverrides.class);
		if (overrides != null && overrides.value() != null)
		{
			for (AttributeOverride attribute : overrides.value())
			{
				if (attribute.name() != null && attribute.name().equals(subProperty.getName()))
				{
					realColumn = attribute.column();
				}
			}
		}
		return realColumn.name();
	}
	private boolean isGetter(Method method)
	{
		boolean isGetter = false;
		if (method.getName().length() > 4 && method.getName().startsWith("get")
				&& (method.getParameterTypes() == null || method.getParameterTypes().length == 0))
		{
			isGetter = true;
		}
		return isGetter;
	}

	private Method getSetterMethod(Class<?> clazz, Method getterMethod)
	{
		String setterMethodName = "s" + getterMethod.getName().substring(1);
		Method retMethod = null;
		try
		{
			retMethod = clazz.getMethod(setterMethodName, getterMethod.getReturnType());
		}
		catch (NoSuchMethodException e)
		{
			e.printStackTrace();
		}
		return retMethod;
	}

	/**
	 * Return class type.
	 *
	 * @return
	 */
	public Class<?> getClazz()
	{
		return clazz;
	}
	/**
	 * Return Table annotation.
	 *
	 * @return
	 */
	public Table getTable()
	{
		return table;
	}	
	/**
	 * Return EmbeddedId annotation.
	 *
	 * @return
	 */
	public AnnotatedProperty getEmbeddedId()
	{
		return embeddedId;
	}

	public AnnotatedProperty getYitianId()
	{
		return this.yitianId;
	}

	/**
	 * Return Id annotation list.
	 *
	 * @return
	 */
	public List<AnnotatedProperty> getIds()
	{
		return ids;
	}
	/**
	 * Get PK columns list.
	 *
	 * @return
	 */
	public String[] getPkColumns()
	{
		//At first, pk colum list is null.
		if (this.pkColumns == null)
		{
			//If domain model extends its PK model
			if (ids != null && !ids.isEmpty())
			{
				pkColumns = getPKColumnByIds(ids);
			}
			//If domain model embeds ID model
			else if (embeddedId != null)
			{
				Class<?> idClass = embeddedId.getType();
				AnnotatedClass clazz = AnnotationConfigurationUtil.getClassAnnotation(idClass);
				Map<String, AnnotatedProperty> allColumnsMap = clazz.getColumnMap();
				pkColumns = new String[allColumnsMap.size()];
				Iterator<String> iter = allColumnsMap.keySet().iterator();
				int i = 0;
				//Loop for each column
				while(iter.hasNext())
				{
					pkColumns[i] = iter.next();
					i = i + 1;
				}
			}
		}
		return pkColumns;
	}

	/**
	 * Get PK column list by IDs.
	 *
	 * @param idList ID list.
	 * @return 
	 */
	private String[] getPKColumnByIds(List<AnnotatedProperty> idList)
	{
		String[] pkColumns = new String[idList.size()];
		//Loop for each ID
		for (int i = 0; i < idList.size(); i++)
		{
			Column column = idList.get(i).getAnnotation(Column.class);
			pkColumns[i] = column.name();
		}
		return pkColumns;
	}


	/**
	 * Return DB column/property pair in domain model.
	 * 
	 * @return
	 */
	public Map<String, AnnotatedProperty> getColumnMap()
	{
		return columnMap;
	}
	/**
	 * Return transient column/property pair in domain model.
	 * 
	 * @return
	 */
	public Map<String, String> getTransientColumnMap()
	{
		return transientColumnMap;
	}
	/**
	 * Return lob column/property pair in domain model.
	 * 
	 * @return
	 */
	public Map<String, String> getLobColumnMap()
	{
		return lobColumnMap;
	}
	
	
	/**
	 * 
	 * Return property by column name.
	 *
	 * @param columnName column name
	 * @return
	 */
	public AnnotatedProperty getPropertyByColumn(String columnName)
	{
		return columnMap.get(columnName);
	}
	
	/**
	 *
	 * Return wrapped property object with annotation.
	 *
	 * @param propertyName	property Name
	 * @return
	 */
	public AnnotatedProperty getAnnotatedPropertyByName(String propertyName)
	{
		return propertyList.get(propertyName);
	}

//	public AnnotatedProperty getColumnAAnnotatedPropertyByName(String propertyName)
//	{
//		return propertyColumnAnnMap.get(propertyName);
//	}
	
	/**
	 * 
	 * Return all embedded properties.
	 *
	 * @return
	 */
	public List<AnnotatedProperty> getEmbeddedProperties()
	{
		return embeddedProperties;
	}
	/**
	 * 
	 * Return all embedded properties.
	 *
	 * @return
	 */
	public List<AnnotatedProperty> getSubModelProperties()
	{
		return this.subModelProperties;
	}
	/**
	 * 
	 * Return all property list.
	 *
	 * @return
	 */
	public Map<String, AnnotatedProperty> getPropertyList()
	{
		return propertyList;
	}
	/**
	 * 
	 * Check to see if the sequence column exist in PK columns.
	 *
	 * @return
	 */
	public boolean isSequenceInPk()
	{
		return this.isSeqenceInPK;
	}


}
