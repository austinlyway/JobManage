package com.lywei.dbtool.annotation;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.Column;

/**
 * <pre>
 */
public class AnnotatedPropertyUtil
{
	/**
	 * Get all column values
	 * 
	 * @param entity	domain model 
	 * @param columnNames	column name array.
	 * @return
	 */
	public static Object[] getPropertyValues(Object entity, String[] columnNames)
	{
		Object[] values = new Object[columnNames.length];
		int index = 0;
		for (String column : columnNames)
		{
			Object value = AnnotatedPropertyUtil.getPropertyValue(entity, column);
			values[index++] = value;
		}
		return values;
	}

	/**
	 * Get property value of domain value with DB column name. It can support embedded/embeddedId annotation.
	 * 
	 * @param entity	domain model
	 * @param columnName	column name
	 * @return
	 */
	public static Object getPropertyValue(Object entity, String columnName)
	{
		// check the input parameter
		AnnotatedClass annotatedClass = null;
		if (entity != null)
		{
			annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(entity.getClass());
		}
		if (entity == null || annotatedClass == null)
		{
			return null;
		}

		Object property = annotatedClass.getPropertyByColumn(columnName);
		Object propertyValue = null;
		// Check to see if the property is embedded object.
		if (property instanceof AnnotatedProperty)
		{
			// It is simple property without embedded annotation.
			propertyValue = ((AnnotatedProperty) property).getValue(entity);
		}
		else
		{
			// It is a embedded object. for example, capTypePK.serviceProviderCode
			String[] properties = ((String) property).split("\\.");
			Object subEntity = entity;

			boolean checkSubProperty = true;
			for (int i = 0; checkSubProperty; i++)
			{
				checkSubProperty = false;

				// check the annotation
				annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(subEntity.getClass());
				if (annotatedClass != null)
				{
					AnnotatedProperty annotatedProperty = annotatedClass.getAnnotatedPropertyByName(properties[i]);

					if (annotatedProperty != null)
					{
						subEntity = annotatedProperty.getValue(subEntity);

						// continue checking if it's not the last property
						checkSubProperty = (i != properties.length - 1 && subEntity != null);
					}
				}
			}
			propertyValue = subEntity;
		}
		return propertyValue;
	}

	/**
	 * Get property value of domain value with DB column name. It can support embedded/embeddedId annotation.
	 * 
	 * @param annotatedClass	annotatedClass
	 * @param columnName	column name
	 * @return
	 */
	public static AnnotatedProperty getPropertyByColumn(AnnotatedClass annotatedClass, String columnName)
	{
		Object property = annotatedClass.getPropertyByColumn(columnName);
		AnnotatedProperty returnProperty = null;
		// Check to see if the property is embedded object.
		if (property instanceof AnnotatedProperty)
		{
			// It is simple property without embedded annotation.
			returnProperty = ((AnnotatedProperty) property);
		}
		else
		{
			// It is a embedded object. for example, capTypePK.serviceProviderCode
			String[] properties = ((String) property).split("\\.");
			AnnotatedProperty parentProperty = annotatedClass.getAnnotatedPropertyByName(properties[0]);
			Class<?> parentClass = parentProperty.getType();
			AnnotatedClass parentAnnotatedClass = AnnotationConfigurationUtil.getClassAnnotation(parentClass);
			returnProperty = parentAnnotatedClass.getAnnotatedPropertyByName(properties[1]);
		}
		return returnProperty;
	}

	/**
	 * Set property value of domain value with DB column name. It can support embedded/embeddedId annotation.
	 * 
	 * @param annotatedClass	annotatedClass of domain model.
	 * @param entity		domain model
	 * @param property		property object, it may be Field object or string. such as AuditModel.name.
	 * @param value 		column value.
	 * @return
	 * @throws Exception 
	 */
	public static void setPropertyValue(AnnotatedClass annotatedClass, Object entity, Object property, Object value) throws Exception
	{
		// Check to see if the property is embedded object.
		if (property instanceof AnnotatedProperty)
		{
			// It is simple property without embedded annotation.
			((AnnotatedProperty) property).setValue(entity, value);
		}
		else
		{
			// It is a embedded object. for example, capTypePK.serviceProviderCode
			String[] properties = ((String) property).split("\\.");
			Object subEntity = entity;
			//Loop for each level 
			for (int i = 0; i < properties.length; i++)
			{
				// check the annotation
				if (annotatedClass == null)
				{
					annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(subEntity.getClass());
				}
				AnnotatedProperty annotatedProperty = annotatedClass.getAnnotatedPropertyByName(properties[i]);
				//Null check
				if (annotatedProperty == null)
				{
					throw new Exception("Invalid property definition in class " + entity.getClass() + " " + property);
				}
				//If it is last one
				if (i == properties.length - 1)
				{
					annotatedProperty.setValue(subEntity, value);
					break;
				}
				//it has a parent model
				else
				{
					Object newSubEntity = annotatedProperty.getValue(subEntity);
					//If new sub entity is null, then new its instance.
					if (newSubEntity == null)
					{
						Class<?> subClass = annotatedProperty.getType();
						newSubEntity = subClass.newInstance();
						annotatedProperty.setValue(subEntity, newSubEntity);
					}
					subEntity = newSubEntity;
				}
				annotatedClass = null;
			}
		}
	}

	/**
	 * Get property value with property. 
	 * It support embedded annotation.
	 *
	 * @param annotatedClass	annotatedClass
	 * @param entity			Object instance
	 * @param property			Property definition.
	 * @return
	 */
	public static Object getPropertyValue(AnnotatedClass annotatedClass, Object entity, AnnotatedProperty property)
	{
		if(entity == null)
		{
			return null;
		}
		Object propertyValue = null;
		// Check to see if the property is embedded object.
		if (property.getParentAnnotatedProperty() == null
				|| property.getClassType() == entity.getClass())
		{
			// It is simple property without embedded annotation.
			propertyValue = property.getValue(entity);
		}
		else
		{
			AnnotatedProperty parentAnnotatedProperty = property.getParentAnnotatedProperty();
			//获取当前属性所对应的类的值.
			Object parentPropertyValue = parentAnnotatedProperty.getValue(entity);
			getPropertyValue(annotatedClass, parentPropertyValue, property);
		}
		return propertyValue;
	}

	/**
	 * Set AccealQuery result to property of domain model.
	 *
	 * @param entity		domain model instance.
	 * @param property		property
	 * @param queryResult	Query result of Accela query.
	 * @return
	 */
	public static Object setPropertyWithQueryResult(Object entity, AnnotatedProperty property, List<?> queryResult)
	{
		//Null check
		if (entity == null || queryResult == null || queryResult.isEmpty() || queryResult.get(0) == null)
		{
			return null;
		}
		Class<?> retType = property.getType();
		Object ret = null;
		//Check to see if the return type is list.
		if (List.class.isAssignableFrom(retType))
		{
			ret = queryResult;
		}
		//Not a list.
		else
		{
			//Try to get first object that is not null. 
			if (queryResult != null && !queryResult.isEmpty())
			{
				ret = queryResult.get(0);
			}
		}
		property.setValue(entity, ret);
		return ret;
	}

	/**
	 * 
	 * Return property value.
	 *
	 * @param entity	domain model
	 * @param field		field object
	 * @return
	 */
	public static Object getPropertyValue(Object entity, Field field)
	{
		if (entity == null || field == null)
		{
			return null;
		}

		Class<?> fieldType = entity.getClass();
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(fieldType);
		AnnotatedProperty annotatedProperty = annotatedClass.getAnnotatedPropertyByName(field.getName());
		Object returnObj = null;
		if (annotatedProperty != null)
		{
			returnObj = annotatedProperty.getValue(entity);
		}
		return returnObj;
	}

	/**
	 * 
	 * Get annotated column of the field in domain model.
	 *
	 * @param entityClass - domain model
	 * @param propertyName - the property Name in domain model 
	 * @return
	 */
	public static String getColumnByProperty(Class<?> entityClass, String propertyName)
	{
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(entityClass);
		AnnotatedProperty annotatedProperty = annotatedClass.getAnnotatedPropertyByName(propertyName);
		String column = null;
		if (annotatedProperty != null)
		{
			Column columnAnnotation = annotatedProperty.getAnnotation(Column.class);
			if (columnAnnotation != null)
			{
				column = columnAnnotation.name();
			}
		}
		return column;
	}
}
