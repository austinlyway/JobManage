package com.lywei.dbtool.jdbc;


import com.lywei.util.EmptyUtil;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DefaultResultConverterImpl implements ResultConverter
{

	public Map<String, Object> convert(ResultSet rs) throws SQLException
	{
		ResultSetMetaData meta = rs.getMetaData();
		int numCols = meta.getColumnCount();
		Map<String, Object> data = new HashMap<>();
		for (int i = 0; i < numCols; i++)
		{
			int currentIndex = i + 1;
			String columnName = meta.getColumnName(currentIndex).trim();
			Object value = rs.getObject(currentIndex);//rs.getObject(columnName);
			if(value instanceof Date){
				value = rs.getTime(columnName);
			}

			if(value != null)
			{
				data.put(columnName, value);
			}
		}
		return data;
	}
}
