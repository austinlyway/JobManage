package com.lywei.dbtool.jdbc;


import com.lywei.dbtool.annotation.AnnotatedClass;
import com.lywei.dbtool.annotation.AnnotatedProperty;

import javax.persistence.Column;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ObjectResultConverterImpl<T> implements ResultConverter
{
	private Class<T> clazzType;

	public ObjectResultConverterImpl()
	{
		clazzType = (Class<T>)getSuperClassGenricType(this.getClass(), 0);
	}

	public ObjectResultConverterImpl(Class<T> clazz)
	{
		this.clazzType = clazz;
	}
	public T convert(ResultSet rs) throws SQLException
	{
		return mapToEntity(rs, clazzType);
	}


	private <T> T mapToEntity(ResultSet rs, Class<T> classType)
	{
		T entity = null;
		try
		{
			AnnotatedClass annotatedClass = new AnnotatedClass(classType);
			entity = classType.newInstance();
			//对象中变量名
			String columnName;

			Field[] feilds = classType.getDeclaredFields();
			for (Field field: feilds)
			{
				AnnotatedProperty annotatedProperty = annotatedClass.getAnnotatedPropertyByName(field.getName());
				if(annotatedProperty != null)
				{
					columnName = annotatedProperty.getAnnotation(Column.class).name();
					Object value = rs.getObject(columnName);
					setValue4Entity(field, entity, converValue2TargetType(value, field.getType()));
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("无法转换DB数据为对象实例," + e.getMessage(), e);
		}
		return entity;
	}

	/**
	 * 根据表字段名和类字段名, 名字一样来匹配设置
	 */
	private <T> Field getMapColumn2FieldByName(String columnName, T entity) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		//对象中变量名
		Field[] fieldArray = entity.getClass().getDeclaredFields();
		Field field = null;
		Field resultField = null;
		int length = fieldArray.length;
		for (int i = 0; i < length; i++)
		{
			field = fieldArray[i];
			if (field.getName().startsWith("ALIAS_") || field.getName().equals("serialVersionUID")) {
				resultField = null;
				continue;
			}
			if (field.getName().equalsIgnoreCase("id")) {
				resultField = null;
				continue;
			}
			if ((columnName.replace("_", "")).equalsIgnoreCase(field.getName())) {
				resultField = field;
				break;
			}
		}
		return resultField;
	}

	/**
	 * 讲数据查询的值,设置到对应的对象实例
	 * @param field
	 * @param entity
	 * @param value
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private void setValue4Entity(Field field, Object entity, final Object value) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		if(value == null)
		{
			return;
		}
		Object realValue = value;
		field.setAccessible(true);
		Class<?> toClazz = field.getType();
		//特殊处理继承自PersistEnum的属性
		field.set(entity, realValue);
	}

	/**
	 * 根据数据库列名,查询数据的数据
	 * @param value
	 * @param toClazz
	 * @return
	 */
	private Object converValue2TargetType(Object value, Class<?> toClazz)
	{
		// 当数据库值的类型与Java目标类型不一致的时候,进行类型转换
		if(toClazz != null && value != null && value.getClass() != toClazz)
		{
			if (toClazz == Double.class)
			{
				value = Double.parseDouble(value.toString());
			}
			if(toClazz == Integer.class)
			{
				value = Integer.parseInt(value.toString());
			}
			if(toClazz == String.class)
			{
				value = String.valueOf(value);
			}
		}
		return value;
	}

	/**
	 * 通过反射, 获得定义Class时声明的父类的泛型参数的类型. 如无法找到, 返回Object.class.
	 *
	 *@param clazz
	 *            clazz The class to introspect
	 * @param index
	 *            the Index of the generic ddeclaration,start from 0.
	 * @return the index generic declaration, or Object.class if cannot be
	 *         determined
	 */
	public Class<Object> getSuperClassGenricType(final Class clazz, final int index) {

		//返回表示此 Class 所表示的实体（类、接口、基本类型或 void）的直接超类的 Type。
		Type genType = clazz.getGenericSuperclass();
		if (!(genType instanceof ParameterizedType)) {
			return Object.class;
		}
		//返回表示此类型实际类型参数的 Type 对象的数组。
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		if (index >= params.length || index < 0) {
			return Object.class;
		}
		if (!(params[index] instanceof Class)) {
			return Object.class;
		}
		return (Class) params[index];
	}

}
