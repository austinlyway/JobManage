package com.lywei.dbtool.sql;


import com.lywei.dbtool.annotation.AnnotatedClass;
import com.lywei.dbtool.annotation.AnnotatedProperty;
import com.lywei.dbtool.annotation.AnnotatedPropertyUtil;
import com.lywei.dbtool.annotation.AnnotationConfigurationUtil;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SQLBuilder
{

	private static final String MISS_ID = "Miss AccelaId annotation in domain model:";
	private static final String MISS_TABLE = "Miss Table annotation in domain model:";

	/**
	 * Create a DB record with domain model.
	 *
	 * @param <T>
	 * @param model			instance of domain model.
	 * @return
	 * @
	 */
	public static <T> SQLObject buildInsertSQL(final T model)
	{
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(model.getClass());
		//
		final Map<String, AnnotatedProperty> columnMap = annotatedClass.getColumnMap();
		final Map<String, String> transientColumnMap = annotatedClass.getTransientColumnMap();
		final Map<String, String> lobColumnMap = annotatedClass.getLobColumnMap();
		final StringBuilder sqlBuilder = new StringBuilder();
		//Generate insert SQL. such as insert into table (column1, column2) values (?,?)
		sqlBuilder.append("INSERT INTO ").append(model.getClass().getAnnotation(Table.class).name()).append('(');
		List<Object> parameters = new ArrayList<Object>();
		Iterator<Map.Entry<String, AnnotatedProperty>> iter = columnMap.entrySet().iterator();
		int columnCount = 0;
		List<Integer> lobColumns = null;
		while (iter.hasNext())
		{
			Map.Entry<String, AnnotatedProperty> entry = iter.next();
			if (transientColumnMap != null && transientColumnMap.containsKey(entry.getKey()))
			{
				continue;
			}
			Object value = getParameterValue(model, annotatedClass, entry.getValue());
			if (value == null)
			{
				continue;
			}
			if (columnCount > 0)
			{
				sqlBuilder.append(',');
			}
			sqlBuilder.append(entry.getKey());
			parameters.add(value);
			if (lobColumnMap != null && lobColumnMap.containsKey(entry.getKey()))
			{
				lobColumns = addLobFlags(columnCount, lobColumns);
			}
			columnCount = columnCount + 1;
		}
		sqlBuilder.append(") VALUES(?");
		for (int i = 1; i < columnCount; i++)
		{
			sqlBuilder.append(",?");
		}
		sqlBuilder.append(")");
		return new SQLObject(sqlBuilder.toString(), parameters);
	}

	public static <T> SQLObject buildUpdateSQL(final T model, final Object originalId, boolean isUpdateNull)
	{
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(model.getClass());
		final Table table = annotatedClass.getTable();
		final Class<?> clazz = model.getClass();
		if (table == null)
		{
			throw new RuntimeException(MISS_TABLE + clazz);
		}
		if (annotatedClass.getPkColumns() == null)
		{
			throw new RuntimeException(MISS_ID + clazz);
		}
		//If PK class is null, it imply that the PK columns will be updated.
		final AnnotatedClass pkClass = getPkClass(originalId);
		//get all columns.
		final Map<String, AnnotatedProperty> columnMap = annotatedClass.getColumnMap();
		//get all transient columns
		final Map<String, String> transientColumnMap = annotatedClass.getTransientColumnMap();
		//Get all LOB columns.
		final Map<String, String> lobColumnMap = annotatedClass.getLobColumnMap();
		final String[] pks = annotatedClass.getPkColumns();
		final Map<String, String> pkMap = createMapFromArray(pks);
		final StringBuilder sqlBuilder = new StringBuilder();
		//Generate insert SQL. such as UPDATE TABLE SET column1 = ?, Column2 =? WHERE PK = ?
		sqlBuilder.append("UPDATE ").append(table.name()).append(" SET ");
		Iterator<Map.Entry<String, AnnotatedProperty>> iter = columnMap.entrySet().iterator();
		int columnCount = 0;
		List<Integer> lobColumns = null;
		final List<Object> parameter = new ArrayList<Object>();
		while (iter.hasNext())
		{
			Map.Entry<String, AnnotatedProperty> entry = iter.next();
			//If the filed is transient, or PK column, we don't need to update it.
			if ((transientColumnMap != null && transientColumnMap.containsKey(entry.getKey()))
					|| (pkMap != null && pkMap.containsKey(entry.getKey())))
			{
				continue;
			}
			Object value = getParameterValue(model, annotatedClass, entry.getValue());
			//当value为空,且不需要更新空值
			if (!isUpdateNull && value == null)
			{
				continue;
			}
			if (columnCount > 0)
			{
				sqlBuilder.append(',');
			}
			sqlBuilder.append(entry.getKey()).append("=?");
			parameter.add(value);
			//If it is LOB column.
			if (lobColumnMap != null && lobColumnMap.containsKey(entry.getKey()))
			{
				lobColumns = addLobFlags(columnCount, lobColumns);
			}
			columnCount = columnCount + 1;
		}
		sqlBuilder.append(" WHERE ");
		//Not update PK columns 
		if (pkClass == null)
		{
			generateWhereSqlUsingColumns(model, annotatedClass, pks, sqlBuilder, parameter);
		}
		//Update PK with new value.
		else
		{
			generateWhereSqlUsingColumns(originalId, pkClass, pks, sqlBuilder, parameter);
		}
		return new SQLObject(sqlBuilder.toString(), parameter);
	}

	public static <T> SQLObject buildDeleteSQL(final T model, final Serializable originalId)
	{
		StringBuilder sqlBuilder = new StringBuilder(200);
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(model.getClass());
		final String[] pks = annotatedClass.getPkColumns();
		sqlBuilder.append("DELETE FROM ").append(model.getClass().getAnnotation(Table.class).name());
		final List<Object> parameter = new ArrayList<Object>();
		sqlBuilder.append(" WHERE ");
		generateWhereSqlUsingColumns(model, annotatedClass, pks, sqlBuilder, parameter);
		return new SQLObject(sqlBuilder.toString(), parameter);
	}

	/**
	 * Generate SQL where clause by column list.
	 *
	 * @param model				domain model			
	 * @param annotatedClass	annotated class of domain model
	 * @param pks				PK column list.
	 * @param sqlBuilder		SQL builder.
	 * @param parameter 		SQL parameters.
	 */
	public static void generateWhereSqlUsingColumns(final Object model, AnnotatedClass annotatedClass, final String[] pks,
			final StringBuilder sqlBuilder, final List<Object> parameter)
	{
		boolean isFirst = true;
		for (String pkColumn : pks)
		{
			if (isFirst)
			{
				isFirst = false;
			}
			else
			{
				sqlBuilder.append(" AND ");
			}
			Object value;
			//If PK is number or string.
			if (model instanceof Number || model instanceof String)
			{
				value = model;
			}
			//PK is a object.
			else
			{
				AnnotatedProperty pkColumnProperty = annotatedClass.getPropertyByColumn(pkColumn);
				value = AnnotatedPropertyUtil.getPropertyValue(annotatedClass, model, pkColumnProperty);
			}
			if (value instanceof Enum<?>)
			{
				value = value.toString();
			}
			if (value == null || "".equals(value))
			{
				sqlBuilder.append("(").append(pkColumn).append("=? OR ").append(pkColumn).append(" IS NULL)");
			}
			else
			{
				sqlBuilder.append(pkColumn).append("=?");
			}
			parameter.add(value);
		}
	}

	/**
	 * Generate SQL query for search by model.
	 *
	 * @param <T>
	 * @param model			domain model
	 * @param clazz			class of domain mdoel
	 * @param parameters	parameter list
	 * @param sqlBuilder	SQL query result.
	 * @throws Exception 
	 * @ 
	 */
	public static <T> void generateSearchSql(T model, Class<T> clazz, final List<Object> parameters, final StringBuilder sqlBuilder) throws Exception
	{
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(clazz);
		final Table table = model.getClass().getAnnotation(Table.class);
		if (table == null)
		{
			throw new Exception(MISS_TABLE + model.getClass());
		}
		//Generate SELECT sql.
		sqlBuilder.append("SELECT * FROM ").append(table.name()).append(" WHERE 1=1");

		final Map<String, AnnotatedProperty> columnMap = annotatedClass.getColumnMap();
		final Map<String, String> transientColumnMap = annotatedClass.getTransientColumnMap();
		Iterator<Map.Entry<String, AnnotatedProperty>> iter = columnMap.entrySet().iterator();
		while (iter.hasNext())
		{
			Map.Entry<String, AnnotatedProperty> entry = iter.next();
			if (transientColumnMap != null && transientColumnMap.containsKey(entry.getKey()))
			{
				continue;
			}
			Object parameter = AnnotatedPropertyUtil.getPropertyValue(annotatedClass, model, entry.getValue());
			//If the property value is not null, then generate SQL where clause automatically.
			if (parameter != null)
			{
				sqlBuilder.append(" AND ").append(entry.getKey()).append("=?");
				parameters.add(parameter);
			}
		}
	}

	/**
	 * Generate SQL query for search by model with has ignore case.
	 *
	 * @param <T>
	 * @param model			domain model
	 * @param clazz			class of domain mdoel
	 * @param parameters	parameter list
	 * @param ignoreCaseFields the fields that want to ignore for search
	 * @param sqlBuilder	SQL query result.
	 * @throws Exception 
	 * @ 
	 */
	public static <T> void generateSearchSqlWithIgnoreCase(T model, Class<T> clazz, final List<Object> parameters, List<String> ignoreCaseFields,
			final StringBuilder sqlBuilder) throws Exception
	{
		AnnotatedClass annotatedClass = AnnotationConfigurationUtil.getClassAnnotation(clazz);
		final Table table = model.getClass().getAnnotation(Table.class);
		if (table == null)
		{
			throw new Exception(MISS_TABLE + model.getClass());
		}
		//Generate SELECT sql.
		sqlBuilder.append("SELECT * FROM ").append(table.name()).append(" WHERE 1=1");

		final Map<String, AnnotatedProperty> columnMap = annotatedClass.getColumnMap();
		final Map<String, String> transientColumnMap = annotatedClass.getTransientColumnMap();
		Iterator<Map.Entry<String, AnnotatedProperty>> iter = columnMap.entrySet().iterator();
		while (iter.hasNext())
		{
			Map.Entry<String, AnnotatedProperty> entry = iter.next();
			if (transientColumnMap != null && transientColumnMap.containsKey(entry.getKey()))
			{
				continue;
			}
			Object parameter = AnnotatedPropertyUtil.getPropertyValue(annotatedClass, model, entry.getValue());
			//If the property value is not null, then generate SQL where clause automatically.
			if (parameter != null)
			{
				if (null == ignoreCaseFields)
				{
					sqlBuilder.append(" AND ").append(entry.getKey()).append("=?");
					parameters.add(parameter);
					continue;
				}

				boolean hasIgnoreCase = false;

				for (String fieldsName : ignoreCaseFields)
				{
					if (entry.getKey().equals(fieldsName))
					{
						hasIgnoreCase = true;
					}
				}

				if (hasIgnoreCase)
				{
					sqlBuilder.append(" AND UPPER (").append(entry.getKey()).append(") = ?");
					parameters.add(((String) parameter).toUpperCase());
				}
				else
				{
					sqlBuilder.append(" AND ").append(entry.getKey()).append("=?");
					parameters.add(parameter);
				}
			}
		}
	}

	/**
	 * Get value from the domain model, and add it into SQL parameters.
	 *
	 * @param <T>		Domain model
	 * @param model		domain model instance.
	 * @param annotatedClass	annotated class of domain model
	 * @param property			The property of field.
	 */
	private static <T> Object getParameterValue(final T model, final AnnotatedClass annotatedClass, final AnnotatedProperty property)
	{
		Object value = AnnotatedPropertyUtil.getPropertyValue(annotatedClass, model, property);
		if (value instanceof Enum<?>)
		{
			value = value.toString();
		}
		return value;
	}

	/**
	 * Get annotated class of PK model.
	 *
	 * @param originalId	PK model.
	 * @return 
	 */
	private static AnnotatedClass getPkClass(final Object originalId)
	{
		AnnotatedClass pkClass = null;
		if (originalId != null)
		{
			pkClass = AnnotationConfigurationUtil.getClassAnnotation(originalId.getClass());
		}
		return pkClass;
	}

	/**
	 * Add LOB flags.
	 *
	 * @param columnNumber	Column number
	 * @param lobColumns	Lob column list.
	 * @return 
	 */
	private static List<Integer> addLobFlags(final int columnNumber, List<Integer> lobColumns)
	{
		if (lobColumns == null)
		{
			lobColumns = new ArrayList<Integer>();
		}
		lobColumns.add(Integer.valueOf(columnNumber));
		return lobColumns;
	}

	/**
	 * Create Map from String array.
	 *
	 * @param columns string array of columns.
	 */
	private static Map<String, String> createMapFromArray(final String[] columns)
	{
		final Map<String, String> columnMap = new HashMap<String, String>();
		for (String column : columns)
		{
			columnMap.put(column, column);
		}
		return columnMap;
	}
}
