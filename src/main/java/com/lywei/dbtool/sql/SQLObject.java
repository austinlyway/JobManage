package com.lywei.dbtool.sql;

import com.lywei.util.EmptyUtil;

import java.util.ArrayList;
import java.util.List;


public class SQLObject
{
	public SQLObject()
	{

	}

	public SQLObject(String sql, List<Object> parameterList)
	{
		this.sql = sql;
		this.parameterList = parameterList;
	}

	private String sql;

	private String selectClause;

	private String fromClause;

	private String outerJoinsAfterFrom;

	private String whereClause;

	private String outerJoinsAfterWhere;

	private String orderByClause;

	private String groupByClause;

	private List<Object> parameterList;

	public String getSqlString()
	{
		StringBuilder sb = new StringBuilder(200);
		sb.append(selectClause).append(fromClause);
		if (EmptyUtil.isNotEmpty(outerJoinsAfterFrom))
		{
			sb.append(outerJoinsAfterFrom);
		}
		sb.append(" WHERE 1 = 1");
		if (EmptyUtil.isNotEmpty(whereClause))
		{
			sb.append(whereClause);
		}
		if (EmptyUtil.isNotEmpty(outerJoinsAfterWhere))
		{
			sb.append(" AND ").append(outerJoinsAfterWhere);
		}
		if (EmptyUtil.isNotEmpty(orderByClause))
		{
			sb.append(" ").append(orderByClause);
		}
		if (EmptyUtil.isNotEmpty(groupByClause))
		{
			sb.append(" ").append(groupByClause);
		}
		return sb.toString();
	}

	public String getSql()
	{
		if (EmptyUtil.isEmpty(sql))
		{
			sql = getSqlString();
		}
		return sql;
	}

	public void setSql(String sql)
	{
		this.sql = sql;
	}

	public String getSelectClause()
	{
		return selectClause;
	}

	public void setSelectClause(String selectClause)
	{
		this.selectClause = selectClause;
	}

	public String getFromClause()
	{
		return fromClause;
	}

	public void setFromClause(String fromClause)
	{
		this.fromClause = fromClause;
	}

	public String getOuterJoinsAfterFrom()
	{
		return outerJoinsAfterFrom;
	}

	public void setOuterJoinsAfterFrom(String outerJoinsAfterFrom)
	{
		this.outerJoinsAfterFrom = outerJoinsAfterFrom;
	}

	public String getWhereClause()
	{
		return whereClause;
	}

	public void setWhereClause(String whereClause)
	{
		this.whereClause = whereClause;
	}

	public String getOuterJoinsAfterWhere()
	{
		return outerJoinsAfterWhere;
	}

	public void setOuterJoinsAfterWhere(String outerJoinsAfterWhere)
	{
		this.outerJoinsAfterWhere = outerJoinsAfterWhere;
	}

	public String getOrderByClause()
	{
		return orderByClause;
	}

	public void setOrderByClause(String orderByClause)
	{
		this.orderByClause = orderByClause;
	}

	public String getGroupByClause()
	{
		return groupByClause;
	}

	public void setGroupByClause(String groupByClause)
	{
		this.groupByClause = groupByClause;
	}

	public Object[] getParameters()
	{
		if (parameterList == null || parameterList.size() == 0)
		{
			return new Object[] {};
		}
		return parameterList.toArray();
	}

	public List<Object> getParameterList()
	{
		return parameterList;
	}

	public void setParameterList(List<Object> parameterList)
	{
		this.parameterList = parameterList;
	}

	public void addParameter(Object obj)
	{
		if (parameterList == null || parameterList.size() == 0)
		{
			parameterList = new ArrayList<Object>();
		}
		parameterList.add(obj);
	}
}
