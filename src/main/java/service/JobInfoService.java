package service;

import com.lywei.batchjob.core.JobInfo;

import java.util.List;

/**
 * Created by austin on 11/25/16.
 */
public interface JobInfoService {

    /**
     * 更新Job的状态
     * @param jobStatus
     * @return
     */
    List<JobInfo> updateJobInStatus(Integer jobStatus);

    /**
     * 根据JobId获取对应的JobInfo
     * @param jobId
     * @return
     */
    JobInfo getJobInfoByJobId(Integer jobId);

    /**
     * 根据状态获取JobInfo
     * @param jobStatus
     * @return
     */
    List<JobInfo> getJobInfoByStatus(Integer jobStatus);

}
