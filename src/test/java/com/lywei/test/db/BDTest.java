package com.lywei.test.db;

import com.lywei.batchjob.core.JobInfo;
import com.lywei.dbtool.JdbcDBAccessor;
import com.lywei.dbtool.jdbc.ObjectResultConverterImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by austin on 11/25/16.
 */
public class BDTest {

    JdbcDBAccessor jdb = new JdbcDBAccessor();

    @Test
    public void testSelect(){
        String sql = "SELECT * FROM opt_batch_job";
        List<JobInfo> list = jdb.query(sql, null, new ObjectResultConverterImpl<>(JobInfo.class));
        Assert.assertNotNull(list);
        JobInfo jobInfo = list.get(0);
        Assert.assertTrue(jobInfo.getJobStatus() == 1);
    }

    @Test
    public void testPah()
    {
        String currentPath1=getClass().getResource(".").getFile().toString();
        System.out.println(currentPath1);
    }
}
